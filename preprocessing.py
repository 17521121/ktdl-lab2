import numpy as np
import pandas as pd
import codecs

data = pd.read_csv("Data/ver2.csv", header = None)

def convertToBinary(header, row):
    result = []
    for i in header:
        t  =  -1
        for j in row:
            if (i == str(j)):
                t = 1
                break
        if(t >= 0):
            result.append("y")
        else:
            result.append("n")
    return np.array(result)

ver3 = codecs.open("Data/plants.csv", "w+", "utf-8")
header = np.unique(data.values[:,1:].astype(str))
header = np.setdiff1d(header, "nan")
ver3.write("%s,%s\n" %("Name", ",".join(header)))
for i in data.values:
    ver3.write("%s,%s\n" % (i[0], ",".join(convertToBinary(header,i[1:]))))
ver3.close()