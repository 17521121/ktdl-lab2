import numpy as np
import pandas as pd
import codecs

data = pd.read_csv("Data/plants.csv")

#convert "n" to "?"
def missingValueToQM(data):
    for iRow, vRow in enumerate(data):
        for iCol, vCol in enumerate(vRow):
            if(vCol == "n"):
                data[iRow][iCol] = "?"
    return data

arff = codecs.open("Data/plants.arff", "w+", "utf-8")
arff.write("@relation plants\n\n")
 
for i,iVal in enumerate(data.columns.values[1:]):
    arff.write("@attribute %s {y}\n" % iVal)

arff.write("\n@data\n")
for i  in missingValueToQM(data.values[:,1:]):
    arff.write("%s\n" %(",".join(i)))
arff.close()